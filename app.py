# -*- coding: utf-8 -*-
"""

"""
import os
import sys
from bs4 import BeautifulSoup
import click as click
import requests
import re


@click.command()
@click.option('--url', help='Url to grab.')
@click.option('--out', help='Url to grab.')
@click.option('--prepend', help='Url to grab.')
def main(url, out, prepend=None):
    if not os.path.exists(out):
        os.mkdir(out)

    os.chdir(out)

    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    a = soup.find_all('a')
    for link in a:
        if re.match('.*mp3$', link.get('href').lower()):
            file_url = str(prepend) + link.get('href')
            print 'Trying to download: ' + file_url
            os.system('axel -n 8 -a ' + file_url)

if __name__ == '__main__':
    sys.exit(main())
