# mp3_downloader
Download from mp3 archive pages

### needs axel from repository

example: 
```
sudo pacman -S axel
```

### initialize virtualenv:
```
virtualenv -p /usr/bin/python2.7 env

source ./env/bin/activate
```

install requirments:
```
./env/bin/pip install -r requirements
```

### and finally run command:
```
./env/bin/python app.py --url {your url} --out {path to folder that files should be put in} --prepend "{if url in page are not full path and the domain name needs to be prepended - care for trailing slashes}"
```

example:
```
./env/bin/python app.py --url "http://dl.12flymusic.com/full/Ebi/" --out "/home/ghost/Music" --prepend "http://dl.12flymusic.com"
```
